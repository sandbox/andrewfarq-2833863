# Caucasus-capitals
This is a custom module. It should be installed in sites/all/modules/custom/.
When you enable it it will install and configure two blocks.
The settings of the blocks can be overriden in the administrative interface.
One is positioned in the sidebar-first region and the other in the
sidebar-second region.

They are configured to display on certain pages and not on others.
One one page they are both displayed.
The first block contains a list of the Countries in the Caucasus
region of Central Asia.
The second block contains a list of the capital cities of these countries.

Menus links are provided for the pages using hook_menu.

The module contains a stylesheet which styles the blocks. Using the render
array, the module adds markup to both blocks. It wraps them in a div tag and 
inserts a custom class in it. The class adds a border to the blocks.
